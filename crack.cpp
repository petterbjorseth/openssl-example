#include "crypto.hpp"
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main() {
  stringstream ss;
  char c = 'A';
  while (c <= 'z') {
    string s = "";
    s += c;
    cout << s << endl;
    if (!Crypto::hex(Crypto::pbkdf2(s, "Saltet til Ola")).compare("ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6")) {
      cout << "Passordet er:" << endl;
      cout << s << endl;
      return 0;
    }
    char i = 'A';
    while (i <= 'z') {
      s = "";
      s += c;
      s += i;
      cout << s << endl;
      if (!Crypto::hex(Crypto::pbkdf2(s, "Saltet til Ola")).compare("ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6")) {
        cout << "Passordet er:" << endl;
        cout << s << endl;
        return 0;
      }
      char j = 'A';
      while (j <= 'z') {
        s = "";
        s += c;
        s += i;
        s += j;
        cout << s << endl;
        if (!Crypto::hex(Crypto::pbkdf2(s, "Saltet til Ola")).compare("ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6")) {
          cout << "Passordet er:" << endl;
          cout << s << endl;
          return 0;
        }
        if (j == 'Z') {
          j = 'a';
          continue;
        }
        j++;
      }
      if (i == 'Z') {
        i = 'a';
        continue;
      }
      i++;
    }
    if (c == 'Z') {
      c = 'a';
      continue;
    }
    c++;
  }
  //passordet er QwE
  return 0;
}